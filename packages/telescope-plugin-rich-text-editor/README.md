# Telescope Simple Rich Text Editor Plugin, Uses Froala Editor

A simple plugin that replaces the standard post body editor with [donchess:autoform-froala][5], [Froala][2] rich text editor for your [Telescope][1] site.  
Due not this is a first version does not offer the full Froala settings or options.  


## Install

```bash
meteor add almogdesign:telescope-plugin-rich-text-editor
```

That's it the plugin will automatically remove the standard post body editor and add the rich text version  

## TeleScope Known Issue

TeleScope currently has a conflict with Autoform. It’s because Telescope uses a customized version of Autoform and any time you install a non-Telescope Autoform plugin it pulls in the regular version as a dependency.

Right now the fix is to clone [donchess:autoform-froala][5] into your app and change its `aldeed:autoform` dependency to `sacha:autoform`

## Customization

Will provide options in future versions. 


## Future
Feature suggestions and pull requests welcome!  Troubles?  Drop me a line at [GitHub][3], need a custom Meteor Package or Application contact me at [Almog Design][4].

[1]: http://www.telescopeapp.org/
[2]: https://www.froala.com/wysiwyg-editor
[3]: https://github.com/almogdesign
[4]: http://almogdesign.net/
[5]: https://github.com/jshimko/telescope-plugin-hero
[6]: https://atmospherejs.com/donchess/autoform-froala