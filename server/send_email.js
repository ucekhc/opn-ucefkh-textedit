sendFlaggedEmail = function(postId, title, userId){
    var user =  Meteor.users.findOne({_id:userId});
    var properties = {
        postId: postId,
        title: title,
        email: user.emails[0].address
    };

    var subject = "Your Post " + title + " Has Been Flagged";
    Telescope.email.buildAndSend(properties.email, subject, 'emailFlaggedPost', properties);
};

