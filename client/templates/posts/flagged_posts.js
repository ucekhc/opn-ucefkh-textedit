Template.posts_flagged.helpers({
    arguments: function () {
        return {
            terms: {
                view: 'posts_flagged',
                flagged:true,
                limit: 10
            }
        }
    }
});
