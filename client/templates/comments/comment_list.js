Template.comment_list.helpers({
  child_comments: function(){
    var post = this;
    var comments = Comments.find({postId: post._id, parentCommentId: null}, {sort: {score: -1, postedAt: -1}});
    return comments;
  },
  isLoggedIn: function () {
    return !!Meteor.user();
  },
  isGuest: function () {
    var user = Meteor.users.findOne();
    if (user) {
      return user.isGuest;
    }
  }
});

Template.comment_list.rendered = function(){
  // once all comments have been rendered, activate comment queuing for future real-time comments
  window.queueComments = true;
};
