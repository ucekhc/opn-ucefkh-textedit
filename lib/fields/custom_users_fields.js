 Users.addField({
    fieldName: 'profile.firstName',
    label: "First Name",
    public: true,
    fieldSchema: {
        type: String,
        optional: false,
        required:true,
        public: true,
        profile: true,
        editableBy: ["member" , "admin"]
    }
});

Users.addField({
    fieldName: 'profile.lastName',
    label: "Last Name",
    public: true,
    fieldSchema: {
        type: String,
        optional: false,
        required:true,
        public: true,
        profile: true,
        editableBy: ["member" , "admin"]
    }
});

Users.addField({
    fieldName: 'profile.position',
    label: "Position",
    public: true,
    fieldSchema: {
        type: String,
        optional: false,
        required:true,
        public: true,
        profile: true,
        editableBy: ["member" , "admin"]
    }
});

Users.addField({
    fieldName: 'profile.business',
    label: "Business",
    public: true,
    fieldSchema: {
        type: String,
        optional: false,
        required:true,
        public: true,
        profile: true,
        editableBy: ["member" , "admin"]
    }
});
/*
Users.addField({
    fieldName: 'profile.bio',
    label: "Bio",
    public: true,
    fieldSchema: {
        type: String,
        min: 20,
        max: 250,
        optional: false,
        required:true,
        public: true,
        profile: true,
        editableBy: ["member" , "admin"],
        autoform: {
            rows: 5
        }
    }
});
*/

Users.addField({
    fieldName: 'profile.city',
    label: "City",
    public: true,
    fieldSchema: {
        type: String,
        optional: false,
        required:true,
        public: true,
        profile: true,
        editableBy: ["member" , "admin"]
    }
});

Users.addField({
    fieldName: 'profile.country',
    label: "Country",
    public: true,
    fieldSchema: {
        type: String,
        optional: false,
        required:true,
        public: true,
        profile: true,
        editableBy: ["member" , "admin"],
        autoform: {
            type: "select2",
            defaultValues: function() {
                var user = Meteor.users.findOne();
                if (user && !_.isEmpty(user.profile.country)) {
                 //   return {value: user.profile.country};//value: 
                    return user.profile.country;
                }
            },
            options: function () {
                return countriesList;
            }
        }
    }
});

Users.addField({
    fieldName: 'profile.terms',
    label: "Newsletter",
    public: true,
    fieldSchema: {
        type: Boolean,
        optional: false,
        required:true,
        public: true,
        profile: true,
        editableBy: ["member" , "admin"],
        autoform: {
            type: "select-checkbox",
            defaultValues: function() {
                var user = Meteor.user();
                if (user && !_.isEmpty(user.profile.terms)) {
                 //   return {value: user.profile.country};//value: 
                    return user.profile.terms;
                }
            },
            options: function () {
                return [
                  {
                    label:"I affirm that I have read the Community Guidelines and will abide by them.",
                    value: true
                    }
                ];
            }
        }
    }
});


Users.addField({
    fieldName: 'isGuest',
    label: "User Type - Guest",
    public: true,
    fieldSchema: {
        type: Boolean,
        //TODO: change back to true
        defaultValue: false,
        optional: false,
        editableBy: ["admin"],
        autoform: {
            omit: true
        }
    }
});

